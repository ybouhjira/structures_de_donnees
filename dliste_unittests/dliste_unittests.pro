#-------------------------------------------------
#
# Project created by QtCreator 2013-10-20T12:28:17
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_dliste_unitteststest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_dliste_unitteststest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
