#include <QString>
#include <QtTest>
#include "../dliste.h"
#include <stdlib.h>

class Dliste_unittestsTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void test_Taille_dliste();
    void test_Inserer_dliste();
    void test_Acceder_dliste();
    void test_Supprimer_dliste();
    void test_Localiser_dliste();
    void test_Raz_dliste();
    void test_Ajouter_fin_dliste();
};

void Dliste_unittestsTest::test_Inserer_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);

    DlisteElement *actuel = liste.premier;
    for (int i = 0; i < 15; ++i, actuel = actuel->suivant ) {
        QCOMPARE(actuel->valeur, i);
    }

    // test 2
    Dliste liste2 = {NULL, NULL};
    QCOMPARE(liste2.premier, (DlisteElement*)NULL);
    QCOMPARE(liste2.dernier, (DlisteElement*)NULL);

    Inserer_dliste(&liste2, 10, 0);
    QCOMPARE(liste2.premier, liste2.dernier);
    QCOMPARE(liste2.premier->valeur, 10);
    QCOMPARE(liste2.dernier->valeur, 10);
}

void Dliste_unittestsTest::test_Taille_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);
    QCOMPARE(Taille_dliste(liste), 15);
}

void Dliste_unittestsTest::test_Acceder_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);
    QCOMPARE(Acceder_dliste(liste, 5).valeur, 5);
}

void Dliste_unittestsTest::test_Supprimer_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);
    Supprimer_dliste(&liste, 5);
    for (int i = 0; i < 5; ++i) {
        QCOMPARE(Acceder_dliste(liste, i).valeur, i);
    }
    for (int i = 5; i < 14; ++i) {
        QCOMPARE(Acceder_dliste(liste, i).valeur, i + 1);
    }

    Dliste liste2 = {NULL, NULL};
    Inserer_dliste(&liste2, 10, 0);
    Supprimer_dliste(&liste2, 0);

    QCOMPARE(liste2.premier, (DlisteElement*)NULL);
    QCOMPARE(liste2.dernier, (DlisteElement*)NULL);
}

void Dliste_unittestsTest::test_Localiser_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);
    QCOMPARE(Localiser_dliste(liste, 5), 5);
    QCOMPARE(Localiser_dliste(liste, 20), -1);
}

void Dliste_unittestsTest::test_Raz_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Inserer_dliste(&liste, i, i);
    Raz_dliste(&liste);
    QCOMPARE(Taille_dliste(liste), 0);
    DlisteElement *nullElem = NULL;
    QCOMPARE(liste.premier, nullElem);
    QCOMPARE(liste.dernier, nullElem);
}

void Dliste_unittestsTest::test_Ajouter_fin_dliste()
{
    Dliste liste = {NULL, NULL};
    for (int i = 0; i < 15; ++i)
        Ajouter_fin_dliste(&liste, i);

    QCOMPARE(Taille_dliste(liste), 15);

    for (int i = 0; i < 15; ++i)
        QCOMPARE(Acceder_dliste(liste, i).valeur, i);

}

QTEST_APPLESS_MAIN(Dliste_unittestsTest)

#include "tst_dliste_unitteststest.moc"
