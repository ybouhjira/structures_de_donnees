#-------------------------------------------------
#
# Project created by QtCreator 2013-09-29T20:47:36
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_tp1_tableaux_unit_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tableau_unittests.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
