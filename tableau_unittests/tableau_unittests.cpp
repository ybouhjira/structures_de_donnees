#include <QtTest>
#include "../tableau.h"

class tableau_unittests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testCreer_tableau();
    void testAjout_tableau();
    void testAjout_tableau2();
    void testSupression_tableau();
    void testRecherche_tableau();
    void testRecherche_dichotomique_tableau();
};

void tableau_unittests::testCreer_tableau()
{
    Tableau tableau = Creer_tableau(100);
    QVERIFY(tableau.taille_max == 100 &&
            tableau.donnee != NULL &&
            tableau.dernier_element == -1);
    Detruire_tableau(&tableau);
}

void tableau_unittests::testAjout_tableau()
{
    Tableau tableau = Creer_tableau(100);

    // Test ajout a la fin
    Ajout_tableau(&tableau ,1 ,0);
    QVERIFY(tableau.donnee[0] == 1);
    QVERIFY(tableau.dernier_element == 0);
    QVERIFY(tableau.taille_max == 100);

    Ajout_tableau(&tableau ,2 ,tableau.dernier_element + 1);
    QVERIFY(tableau.donnee[0] == 1);
    QVERIFY(tableau.donnee[1] == 2);
    QVERIFY(tableau.dernier_element == 1 );
    QVERIFY(tableau.taille_max == 100);

    Ajout_tableau(&tableau ,3 ,tableau.dernier_element + 1);
    QVERIFY(tableau.donnee[0] == 1);
    QVERIFY(tableau.donnee[1] == 2);
    QVERIFY(tableau.donnee[2] == 3);
    QVERIFY(tableau.dernier_element == 2);
    QVERIFY(tableau.taille_max == 100);

    // Test insertion au milieu
    Ajout_tableau(&tableau, 4, 1);
    QString output = "";
    for (int i = 0; i < tableau.dernier_element + 1; ++i)
        output += QString::number(tableau.donnee[i]) + ", ";
    QVERIFY2(output == "1, 4, 2, 3, ", output.toStdString().c_str() );

    // Test ajout au debut
    Ajout_tableau(&tableau, 9, 0);
    Ajout_tableau(&tableau, 8, 0);
    Ajout_tableau(&tableau, 7, 0);
    QString output2 = "";
    for (int i = 0; i < tableau.dernier_element + 1; ++i) {
        output2 += QString::number(tableau.donnee[i]) + ", ";
    }
    QVERIFY2(output2 == "7, 8, 9, 1, 4, 2, 3, ", output.toStdString().c_str() );


    Detruire_tableau(&tableau);
}

void tableau_unittests::testAjout_tableau2()
{
    Tableau tableau = Creer_tableau(100);
    for (int i = 1; i <= 4; ++i)
        Ajout_tableau(&tableau, i, tableau.dernier_element + 1);

    Ajout_tableau(&tableau, 9, tableau.dernier_element + 5);

    QVERIFY(tableau.dernier_element == 8);
    QVERIFY(tableau.donnee[8] == 9);
}

void tableau_unittests::testSupression_tableau() {
    Tableau tableau = Creer_tableau(100);
    for (int i = 1; i < 8; ++i)
        Ajout_tableau(&tableau, i, tableau.dernier_element + 1);
    // 1, 2, 3, 4, 5, 6, 7,

    Suppression_tableau(&tableau, 1);
    Suppression_tableau(&tableau, 1);
    Suppression_tableau(&tableau, 3);
    Suppression_tableau(&tableau, 0);
    Suppression_tableau(&tableau, tableau.dernier_element);
    // 4, 5,

    QString contenu = "";
    for (int i = 0; i <= tableau.dernier_element ; ++i)
        contenu += QString::number(tableau.donnee[i]) + ", " ;

    QVERIFY2(contenu == "4, 5, ", contenu.toStdString().c_str());

    Detruire_tableau(&tableau);
}

void tableau_unittests::testRecherche_tableau()
{
    Tableau tableau = Creer_tableau(100);
    Ajout_tableau(&tableau, 1, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 2, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 2, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 5, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 6, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 8, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 4, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 2, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 1, tableau.dernier_element + 1);

    Tableau resultat = Recherche_tableau(tableau, 2);
    QString contenu = "";
    for (int i = 0; i <= resultat.dernier_element ; ++i)
        contenu += QString::number(resultat.donnee[i]) + ", " ;
    QVERIFY2(contenu == "1, 2, 7, ", contenu.toStdString().c_str());

    Detruire_tableau(&resultat);
    Detruire_tableau(&tableau);
}

void tableau_unittests::testRecherche_dichotomique_tableau()
{
    Tableau tableau = Creer_tableau(100);
    Ajout_tableau(&tableau, 1, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 2, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 3, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 4, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 4, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 5, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 5, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 6, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 7, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 10, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 11, tableau.dernier_element + 1);
    Ajout_tableau(&tableau, 12, tableau.dernier_element + 1);

    Tableau resultat = Recherche_dichotomique_tableau(tableau, 4, 0,
                                                 tableau.dernier_element);
    QString contenu = "";
    for (int i = 0; i <= resultat.dernier_element ; ++i)
        contenu += QString::number(resultat.donnee[i]) + ", " ;
    QVERIFY2(contenu == "3, 4, ", contenu.toStdString().c_str());

    Detruire_tableau(&resultat);
    Detruire_tableau(&tableau);
}

QTEST_APPLESS_MAIN(tableau_unittests)

#include "tableau_unittests.moc"
