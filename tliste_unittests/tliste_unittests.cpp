#include <QString>
#include <QtTest>
#include "../tliste.h"

bool operator ==(Tliste liste1, Tliste liste2)
{
    if(liste1.dernier_element != liste2.dernier_element)
        return false;

    for (int i = 0; i <= liste1.dernier_element; ++i)
        if(liste1.donnee[i] != liste2.donnee[i])
            return false;

    return true;
}

class tliste_unittests : public QObject
{
    Q_OBJECT


private Q_SLOTS:
    void test_Inserer_tliste();
    void test_Localiser_tliste();
    void test_Acceder_tliste();
    void test_Supprimer_tliste();
    void test_Taille_tliste();
};

void tliste_unittests::test_Inserer_tliste()
{
    const int size = 100;
    Tliste tliste = Creer_tliste(size);
    for (int i = 0; i < size; ++i)
        Inserer_tliste(&tliste, i, 0);
    for (int i = 0; i < size; ++i)
        QCOMPARE(tliste.donnee[i], size - i - 1);
}

void tliste_unittests::test_Localiser_tliste()
{
    Tliste tliste = Creer_tliste(100);
    for (int i = 0; i < 15; ++i)
        Inserer_tliste(&tliste, i, i);
    QCOMPARE(Localiser_tliste(tliste, 5), 5);
    QCOMPARE(Localiser_tliste(tliste, 30), -1);
}

void tliste_unittests::test_Acceder_tliste()
{
    Tliste tliste = Creer_tliste(100);
    for (int i = 0; i < 15; ++i)
        Inserer_tliste(&tliste, i, i);
    QCOMPARE(Acceder_liste(tliste, 5), 5);
    QCOMPARE(Acceder_liste(tliste, 3), 3);
}

void tliste_unittests::test_Supprimer_tliste()
{
    Tliste tliste = Creer_tliste(100);
    for (int i = 0; i < 15; ++i)
        Inserer_tliste(&tliste, i, i);
    Supprimer_tliste(&tliste, 5);

    Tliste tliste2 = Creer_tliste(100);
    for (int i = 0; i < 5; ++i)
        Inserer_tliste(&tliste2, i, i);
    for (int i = 6; i < 15; ++i)
        Inserer_tliste(&tliste2, i, i - 1);

    QCOMPARE(tliste, tliste2);
}

void tliste_unittests::test_Taille_tliste()
{
    Tliste tliste = Creer_tliste(100);
    for (int i = 0; i < 15; ++i)
        Inserer_tliste(&tliste, i, i);
    QCOMPARE(Taille_tliste(tliste), 15);

}

QTEST_APPLESS_MAIN(tliste_unittests)

#include "tliste_unittests.moc"
