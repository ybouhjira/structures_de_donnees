#-------------------------------------------------
#
# Project created by QtCreator 2013-10-19T20:55:12
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tliste_unittests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tliste_unittests.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
