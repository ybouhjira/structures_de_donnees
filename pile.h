#ifndef PILE_H
#define PILE_H

#include "dliste.h"
typedef Dliste Pile;

#define Empiler_pile(pile, element) Inserer_dliste(p, element, 0)

#define Depiler_pile(pile, element) Supprimer_dliste(pile, 0)

#define Raz_pile Raz_dliste

#define Sommet_pile(pile) Acceder_dliste(pile, 0)

#define Vide_pile(pile) Taille_dliste(pile) == 0

#endif // PILE_H
